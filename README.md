# OE Scriber Server (broker/publisher implementations) #

The OE Scriber server is intended to facilitate the development of real-time dashboards by publishing lightweight real-time data feeds to Javascript Scriber clients via WebSockets. The name 'Scriber' is short for 'subscriber' - a reference to the publish/subscribe pattern employed here.

**Maintainer:** Michael Bironneau (michael.bironneau@openenergi.com)

### What is a Scriber cluster? ###
Scriber is a brokered-discovery, brokerless-thereafter pub/sub architecture intended for use as a backend for real-time dashboards and web applications. A Scriber cluster consists of a *Broker* and one or more *Publishers*. The Broker just serves as a record of what publishers are available. When a subscriber (client) connects to the broker, it receives the most recent list of available publishers, and it can then connect each publisher and discover its feeds, optionally subscribing to any or all of them. The publisher is responsible for making sure that subscribers always have the latest data for the feeds they are subscribed to.

### How is Scriber different from other Pub/Sub frameworks?
In two ways: Scriber feeds are super-lightweight and work out of the box with web clients (on modern browsers) with *no* additional middleware. 

There are also some extra features that are not always supported by other frameworks: the **replay** feature pushes the latest value of a feed whenever you subscribe, so you don't have to wait for the next update. There is **wildcard** support on subscriptions, which behaves like UNIX filename pattern matching. Scriber clusters use a broker for publisher discovery but are **brokerless** thereafter, so the broker is not a possible bottleneck for the application.

Scriber feeds are *very* lightweight because they are only updated when necessary. Unlike traditional pub/sub pattern where publishers just emit messages, oblivious to the number of subscribers (there may be none), Scriber does not update data feeds unless clients are subscribed to them, which means you can have thousands of data feeds for a very low overhead. 

Like most other frameworks, publishers can be distributed and topics replicated between different publishers, allowing you to distribute update work and add reliability to the publication process.

Scriber publishers use the Websocket protocol to publish messages, which means that you can just use some simple Javascript (see the Scriber client library docs) to subscribe to them. Unlike most pub/sub frameworks, you don't need a middleware layer between Scriber and your client (there is no authentication in place but there can be if and when a need for it arises). 

### How do I get set up? ###

Clone the repository and run `python publisher.py -broker="rlteclab1:7002"`. A command window should pop up displaying various messages. To test the networking bit, open wstest.html in Chrome, right-click anywhere on the blank page and click on 'inspect'. You should see lines corresponding to published JSON objects being printed to the console.

There is a broker running at `rlteclab1:7002` but you can run one on your machine - just make sure to specify it in any client code (for the Javascript library, set `OEScriber.brokerURL`.)

You can run your own publisher with any number of data feeds by running `python publisher.py` (optionally specifying a port number and a broker URL, e.g. `python publisher.py -port=1234 -broker="rlteclab1:7002"`. The default options are port 7001 and a broker located at localhost:7002. 

### Contribution guidelines ###

Most likely you want to create a feed. A feed is basically a class with a single useful method: `update`. This gets the latest value for your topic and updates `self.value` with the result. There are numerous examples in `/feeds`, but for now, here is an example of a feed that just returns the current time:

	import datetime
	from abc import abstractproperty
	from feeds.__base import feed

	class time_feed(feed):
		"""Feed that displays the current time"""
		def get_topic(self):
			"""Unique topic identifier"""
			return 'time'

		def get_timer(self):
			"""How often the feed should update (in seconds)"""
			return 5

		topic = abstractproperty(fget=get_topic)
		timer = abstractproperty(fget=get_timer)

		def update(self):
			"""Update feed"""
			v = datetime.datetime.now().strftime("%B %d, %Y at %I:%M%p")
			if v != self.value:
				return v #Only return a value if it is different from the previous one
						 #self.value is updated after update() returns so we don't need to do it here

**Important:** When you are done creating your feed, go into `feeds\__init__.py` and add it to the list of `import`'ed modules. 

####Testing your feed####
Run `python server.py -broker="rlteclab1:7002"`. If no error messages appear and you see a line telling you that your feed has been discovered and scheduled, you're on the right track. To check that it is updating as expected, open `wstest.html` in Chrome and use the Console to inspect received messages - there should be a "discovery" message corresponding to your feed and you should also see its messages there.

**Note:** There is a known issue with the scheduler that may cause messages to appear more than once. This is because when many messages are scheduled to run at the same (or almost the same) time, a slight delay may occur, leading to two scheduled updates for the same topic occuring almost at the same time. This will get fixed in future versions but for now just ignore it.

### Future work ###
The current version of Scriber is a prototype that is not expected to scale above medium-sized applications. It also has issues with message duplication (which is not necessarily a bad thing in some cases but needs to be avoided in others). Future work will focus on adding reliability, improving the performance and scalability of the system (probably using nanomsg when Websocket transport gets added, should be in the near future according to the mailing list).