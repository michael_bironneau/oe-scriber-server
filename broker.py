"""
OE Scriber broker Server
===========================

:author: Michael Bironneau (michael.bironneau@openenergi.com)

Accepts publisher registration/unregistration requests and tells clients which publishers are currently registered when they request that information.

NOTE: No locking is necessary as we are not using Tornado's asynchronous capabilities, therefore all updates to PUBLISHERS are done from one thread.
"""
import tornado.ioloop
import tornado.web
import json
from tornado.options import define, options
import logging
import logging.config

#Globals
PUBLISHERS = []
define("port", default=7002, help="port to listen on", type=int)

class application(tornado.web.Application):
    """Handles requests"""
    def __init__(self):
        handlers = [
            (r"/register/", registration_handler),
            (r"/unregister/", unregistration_handler),
            (r"/", info_request_handler)
        ]
        tornado.web.Application.__init__(self, handlers)

class registration_handler(tornado.web.RequestHandler):
	def get(self):
		try:
			remote_port = int(self.get_argument('port'))
		except:
			self.set_status(400)
		for p in PUBLISHERS:
			if p['address'] == self.request.remote_ip:
				if p['port'] == str(remote_port):
					logging.info('Duplicate publisher registration request ignored.')
					self.set_status(200)
					return
		PUBLISHERS.append({'address': self.request.remote_ip, 'port': str(remote_port)})
		logging.info('Added publisher: ' + self.request.remote_ip)
		self.set_status(200)

class unregistration_handler(tornado.web.RequestHandler):
	def get(self):
		for p in PUBLISHERS:
			if p['address'] == self.request.remote_ip:
				PUBLISHERS.remove(p)
				logging.info('Removed publisher: ' + self.request.remote_ip)
				self.set_status(200)
				return
		logging.warn('Could not find publisher to remove: ' + self.request.remote_ip)

class info_request_handler(tornado.web.RequestHandler):
	def set_default_headers(self):
		self.set_header('Access-Control-Allow-Origin', '*')
		
	def get(self):
		self.write(json.dumps(PUBLISHERS))
		logging.info('Served request from client: ' + self.request.remote_ip)

if __name__ == "__main__":
    tornado.options.parse_command_line()
    logging.basicConfig(level=logging.INFO)
    logging.info('Starting up...')
    app = application()
    app.listen(options.port)
    logging.info('Listening on port ' + str(options.port))
 #Blocks until CTRL+C (CTRL+Break on Windows) is pressed
    try:
       tornado.ioloop.IOLoop.instance().start()
    except (KeyboardInterrupt, SystemExit):
        pass