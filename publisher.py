"""
OE Scriber Publisher Server
===========================

:author: Michael Bironneau (michael.bironneau@openenergi.com)

The purpose of Scriber (short for 'subscriber') is to provide a scalable, lightweight fabric for a real-time API. Scriber clients subscribe via WebSockets (by default) to certain topics and receive published messages to those topics. This is done with no locking, which makes it faster in most cases, but can occasionally lead to a client receiving the same message more than once when the client turnover is high. This is an acceptable trade off.

Brokered pub/sub pattern
--------------------------
Scriber publishers connect to a Scriber broker on startup and are added to the server pool there. All publishers are not required to publish the same topics, though some degree of overlap would be a good idea to provide a more robust service in case a server goes down unexpectedly. Clients connect to the broker, get a list of available publishers, and then go about connecting to one or more publishers and discovering available FEEDS and/or subscribing to them.

Future work
-----------
In the future a fallback to long-polling via an HTTP endpoint may also be supported for the benefit of the Javascript client library (and those poor buggers stuck supporting old versions of IE).

Another possibility is for a Thrift RPC endpoint but use cases for this are limited, especially with the upcoming OData API.

"""

import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
from tornado.options import define, options
import json
import logging
from apscheduler.schedulers.tornado import TornadoScheduler
from fnmatch import fnmatch
import inspect
import sys
import urllib.request
import feeds

#Globals
define("port", default=7001, help="port to listen on", type=int)
define("broker", default="localhost:7002", help="broker name:port", type=str)
TOPIC_NAMES = [] #Constructed feed message
TOPIC_SUBSCRIPTION_COUNT = {} #Count of subscribers per topic
FEEDS = [] #List of feed classes
SCHEDULER = None #Scheduler for feed updates

class application(tornado.web.Application):
    """Handles requests"""
    def __init__(self):
        handlers = [
            (r"/", ws_handler)
        ]
        tornado.web.Application.__init__(self, handlers)

class ws_handler(tornado.websocket.WebSocketHandler):
    """Handles WebSocket requests"""
    subscribers = set()

    def check_origin(self, origin):
        """Bind to any interface"""
        return True

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request)
        self.__subscribed_topics = set()

    def get_compression_options(self):
        #TODO (Michael): Either make this use gzip or use Msgpack
        #by default instead of json.
        return {}

    def open(self):
        """Once the subscriber is connected, send a list of available topics."""
        ws_handler.subscribers.add(self)
        self.write_message(json.dumps({'topics': TOPIC_NAMES}))

    def on_close(self):
        """Remove subscriber from list and decrement count on relevant topics"""
        ws_handler.subscribers.remove(self)
        for t in self.__subscribed_topics:
            dec_subs(t)
        print('-Connection to client closed-')

    def on_message(self, message):
        """Messages can be of two kinds:
            SUBSCRIBE [topic|*]
            UNSUBSCRIBE [topic|*]
        The * means 'all FEEDS'. Note that topic.* is not the same as topic!
        Anything else will be silently ignored (but logged). Everything is case-sensitive!
        """
        cmds = str(message).strip().split(' ')
        if len(cmds) != 2:
            print('Invalid message structure: ' + str(message))
            return
        if cmds[0].strip() == 'SUBSCRIBE':
            self.subscribe(cmds[1].strip())
        elif cmds[0].strip() == 'UNSUBSCRIBE':
            self.unsubscribe(cmds[1].strip())
            print('-Unsubscribed-')
        else:
            print('Unknown command: ' + str(cmds[0]))

    def subscribe(self, topic):
        self.__subscribed_topics.add(topic)
        inc_subs(topic)
        self.replay_last(topic)
        print('-Client subscribed to ' + topic + '-')

    def unsubscribe(self, topic):
        self.__subscribed_topics.remove(topic)
        dec_subs(topic)
        print('-Client unsubscribed from ' + topic + '-')

    def is_subscribed(self, topic):
        for scribed_topic in self.__subscribed_topics:
            if fnmatch(topic, scribed_topic):
                return scribed_topic

    def publish_to(self, topic, message):
        sub = self.is_subscribed(topic) #Subscription, possibly containing wildcard(s)
        if sub is not None:
            self.write_message(json.dumps({'topic': topic, 'message': message, 'subscription': sub}))


    def replay_last(self, subscription):
        """Replay latest message to client when they subscribe (if there is a message)"""
        if '.' in subscription:
            sub = subscription[0:subscription.find('.')]
        else:
            sub = subscription
        for f in FEEDS:
            if subscription == '*':
                for subtopic, subvalue in f.replay():
                    self.write_message(json.dumps({'topic': subtopic, 'message': subvalue, 'subscription': subscription}))
            else:
                if sub == f.topic:
                   for subtopic, subvalue in f.replay():
                        self.publish_to(subtopic, subvalue)



    def publish(topic, message):
        for scriber in ws_handler.subscribers:
            try:
                scriber.publish_to(topic, message)
            except:
                #This will happen if subscriber is removed from a different thread while
                #attempting to publish a message - it's not very elegant but it's MUCH
                #faster than locking src_handler.subscribers up with reader-writer locks.
                #TODO (Michael): Better handling for different kinds of exceptions
                print('Failed to publish message!')

def inc_subs(topic):
    """Increment topic subscription count
    This is not thread-safe. Only call it from the main thread.
    """
    try:
        if topic == '*':
            for k in TOPIC_SUBSCRIPTION_COUNT.keys():
                TOPIC_SUBSCRIPTION_COUNT[k] += 1
        else:
            if '.' in topic:
                TOPIC_SUBSCRIPTION_COUNT[topic[0:topic.find('.')]] += 1
            else:
                TOPIC_SUBSCRIPTION_COUNT[topic] += 1
    except KeyError: #Subscribing to unknown topic?
        if '.' in topic:
            TOPIC_SUBSCRIPTION_COUNT[topic[0:topic.find('.')]] = 1
        else:
            TOPIC_SUBSCRIPTION_COUNT[topic] = 1

def dec_subs(topic):
    """Decrement topic subscription count
    This is not thread-safe. Only call it from the main thread.
    """
    try:
        if topic == '*':
            for k in TOPIC_SUBSCRIPTION_COUNT.keys():
                TOPIC_SUBSCRIPTION_COUNT[k] -= 1
        else:
            if '.' in topic:
                    TOPIC_SUBSCRIPTION_COUNT[topic[0:topic.find('.')]] -= 1 if TOPIC_SUBSCRIPTION_COUNT[topic[0:topic.find('.')]] > 0 else 0
            else:
                TOPIC_SUBSCRIPTION_COUNT[topic] -= 1 if TOPIC_SUBSCRIPTION_COUNT[topic] > 0 else 0 #Should never be less than 0 but who knows...
    except KeyError: #Trying to unsubscribe from unknown topic?
        if '.' in topic:
                    TOPIC_SUBSCRIPTION_COUNT[topic[0:topic.find('.')]] = 0
        else:
            TOPIC_SUBSCRIPTION_COUNT[topic] = 0

def get_sub_count(topic):
    """Return subscriber count"""
    try:
        return TOPIC_SUBSCRIPTION_COUNT[topic]
    except KeyError:
        return 0 #Unknown topic

def discover_feeds():
    """Get list of all FEEDS
    Scriber publishers maintain a list of instantiated feed classes (global var feed)
    and a list of topic names (TOPIC_NAMES)
    """
    global FEEDS
    global TOPIC_NAMES
    global TOPIC_SUBSCRIPTION_COUNT
    try:
        #Get list of submodules
        for name, cls in inspect.getmembers(feeds):
            if not name.startswith('__'):
                for subname, subcls in inspect.getmembers(cls):
                    if inspect.isclass(subcls):
                        if subname != 'feed' and issubclass(subcls, feeds.__base.feed):
                            #print(subcls().topic + ' is a feed.')
                            feed_cls = subcls()
                            FEEDS.append(feed_cls)
                            TOPIC_NAMES.append(feed_cls.topic)
                            TOPIC_SUBSCRIPTION_COUNT[feed_cls.topic] = 0
                            print('Loaded feed \'' + feed_cls.topic + '\'')
        return True
    except:
        print('Error discovering FEEDS')
        print(sys.exc_info())
        return False

def print_sub_count():
    """Print subscription count to console"""
    print('---------------------')
    print('     Sub counts:     ')
    print('---------------------')
    for k,v in TOPIC_SUBSCRIPTION_COUNT.items():
        print(k + ': ' + str(v))

def launch_updaters():
    """Start scheduled updates"""
    global SCHEDULER
    if SCHEDULER is not None:
        #Scheduler is already running
        try:
            SCHEDULER.shutdown() #Attempt graceful exit
        except:
            print('Had to kill previous SCHEDULER. Not great...')
            SCHEDULER = None
    SCHEDULER = TornadoScheduler()
    for f in FEEDS:
        try:
            SCHEDULER.add_job(f.get_latest, 'interval', seconds=f.timer, args=[ws_handler.publish, get_sub_count])
        except:
            print('Error adding job')
    SCHEDULER.add_job(print_sub_count, 'interval', seconds=60) #Print subscription count every minute
    SCHEDULER.start()

def register_with_broker(broker_name, port):
    """Send a message to broker signalling availability.
    This is done by sending a GET request to [broker url]/register
    """
    try:
        urllib.request.urlopen("http://" + broker_name + '/register/?port=' + str(port)).read()
        return True
    except:
        print('Could not connect to broker!')
        print(sys.exc_info())
        return False

def unregister_with_broker(broker_name):
    """Send a message to broker to signal unavailability.
    This is done by sending a GET request to [broker url]/unregister
    """
    try:
        urllib.request.urlopen("http://" + broker_name + '/unregister/').read()
        return True
    except:
        print('Could not connect to broker!')
        print(sys.exc_info())
        return False

if __name__ == "__main__":
    logging.getLogger('apscheduler.executors').setLevel(logging.WARN)
    logging.getLogger('apscheduler.scheduler').setLevel(logging.WARN)
    tornado.options.parse_command_line()
    if not discover_feeds():
        sys.exit('-Exiting-')
    if not register_with_broker(options.broker, options.port):
        sys.exit('-Exiting-')
    app = application()
    app.listen(options.port)
    launch_updaters()


    #Blocks until CTRL+C (CTRL+Break on Windows) is pressed
    try:
       tornado.ioloop.IOLoop.instance().start()
    except (KeyboardInterrupt, SystemExit):
        unregister_with_broker(options.broker)
        print('Unregistered - exiting.')