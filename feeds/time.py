import datetime
from abc import abstractproperty
from feeds.__base import feed, feed_value

class time_feed(feed):

	def getTopic(self):
		return 'time'
	def getTimer(self):
		return 5

	topic = abstractproperty(fget=getTopic)
	timer = abstractproperty(fget=getTimer)

	def update(self):
		return feed_value(date=datetime.datetime.now().strftime("%B %d, %Y"), 
						time=datetime.datetime.now().strftime("%I:%M%p"))