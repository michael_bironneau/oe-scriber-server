import openenergi as oe
from abc import abstractproperty
from feeds.__base import feed

class online_sites_feed(feed):

	def __init__(self):
		super().__init__()
		self.db = oe.g3()
		self.SQL = "Select Count(*) From vw_SiteList Where DeviceOeStatus = 'Reporting'"

	def getTopic(self):
		return 'onlineSites'
	def getTimer(self):
		return 10

	topic = abstractproperty(fget=getTopic)
	timer = abstractproperty(fget=getTimer)

	def update(self):
		v = self.db.sql(self.SQL).values[0][0]
		return int(float(v))