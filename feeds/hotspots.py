import openenergi as oe
from abc import abstractproperty
from feeds.__base import feed
import csv
import os

class hotspots_feed(feed):

	def __init__(self):
		super().__init__()
		self.db = oe.g3()
		self.HotspotAvailable = """Select SUBSTRING(location.postcode,1,(CHARINDEX(' ',location.postcode + ' ')-1)), Sum(isnull(loLow.KwValue,0)) + Sum(isnull(lohigh.kwvalue,0)) From Load
								Left Join LoadEvent lolow on lolow.loadeventid in (select top 1 loadeventid from loadevent where loadid = load.loadid and loadeventtypeid = 1 order by timestamp desc)
								Left Join LoadEvent lohigh on lohigh.loadeventid in (select top 1 loadeventid from loadevent where loadid = load.loadid and loadeventtypeid = 2 order by timestamp desc)
								inner join device on device.deviceid = load.deviceid
								inner join site on site.siteid = device.siteid
								Inner join location on location.locationid = site.locationid
								group by SUBSTRING(location.postcode,1,(CHARINDEX(' ',location.postcode + ' ')-1))
								"""
		self.HotspotResponse = """Select SUBSTRING(location.postcode,1,(CHARINDEX(' ',location.postcode + ' ')-1)), Sum(abs(isnull(loresp.KwValue,0))) From Load
								Left Join LoadEvent loresp on loresp.loadeventid in (select top 1 loadeventid from loadevent where loadid = load.loadid and loadeventtypeid = 4 order by timestamp desc)
								inner join device on device.deviceid = load.deviceid
								inner join site on site.siteid = device.siteid
								Inner join location on location.locationid = site.locationid
								group by SUBSTRING(location.postcode,1,(CHARINDEX(' ',location.postcode + ' ')-1))"""
		self.scale_x = 0.00154640
		self.scale_y = 0.00109394
		self.translate_x = -13.6913143
		self.translate_y = 49.90961

		with open(os.path.join(os.path.realpath(''), 'data', 'postcodes.csv'), 'r') as f:
			reader = csv.reader(f)
			self.postcode_data = list(reader)

	def getTopic(self):
		return 'hotspots'
	def getTimer(self):
		return 30

	topic = abstractproperty(fget=getTopic)
	timer = abstractproperty(fget=getTimer)

	def getHotspots(self):
		"""Returns dictionary {Available: [{"type":"Point","coordinates":[7398,3284],"properties":{"Value":xxxx}}], Provided: [xxxx]}"""
		ret = {}
		ret['Available'] = list(map(lambda x: {"type": "Point", "coordinates": self.lookupCoordinates(x[0]), "properties": {"value": float(x[1])}}, self.db.sql(self.HotspotAvailable).values))
		ret['Provided'] = list(map(lambda x: {"type": "Point", "coordinates": self.lookupCoordinates(x[0]), "properties": {"value": float(x[1])}}, self.db.sql(self.HotspotResponse).values))
		return ret

	def wrapHotspots(self, hs):
		"""Wraps hotspots in 'topology' node"""
		return {"type": "Topology", 
			"transform": {
				"scale": [
					0.001546403012701271,
					0.0010939367048704803
					],
				"translate": [
					-13.69131425699993,
					49.90961334800009
					]
				},
				"objects": 
					{"available":{"type":"GeometryCollection","geometries": hs['Available']},
					"provided": {"type": "GeometryCollection", "geometries": hs['Provided']}}}

	def lookupCoordinates(self, postcode):
		"""Looks up postcode and returns translated lat/long for first prefix that matches postcode"""
		for p in self.postcode_data:
			if p[0] in postcode and p[0][0].upper() == postcode[0].upper():
				return self.projectCoordinates([float(p[1]), float(p[2])])
		return [0,0] #not found

	def projectCoordinates(self, coordinates):
		"""Translates/scales"""
		return [(coordinates[1]-self.translate_x)/self.scale_x, (coordinates[0]-self.translate_y)/self.scale_y]

	def update(self):
		return self.wrapHotspots(self.getHotspots())