from abc import abstractproperty
from feeds.__base import feed, feed_value
import urllib.request
import json

class localsim(feed):
	"""Obtain a feed of simulated population from an OE Simulator instance running on localhost.

	To set this up you need to run OE Simulator with something like this:

	python run.py 200 40 --csenable --hub scriber
	"""

	feedurl = 'localhost:7003' #URL of KPIs feed
	csurl = 'localhost:8080' #URL of CentralSignal

	def getTopic(self):
		return 'localSim'
	def getTimer(self):
		return 1

	topic = abstractproperty(fget=getTopic)
	timer = abstractproperty(fget=getTimer)

	def update(self):
		"""Return both an availability and central signal feed

		Available topics

		stats
		-------
		gridFrequency
		availableHigh
		availableLow
		response
		meanLoadstate
		controllability
		totalEvents
		"""
		req1 = urllib.request.Request(
			"http://" + localsim.feedurl,
			data=None,
			headers= {'User-Agent': 'Scriber/0.1'}
			)
		f = urllib.request.urlopen(req1)
		sim_vars = json.loads(f.read().decode('utf-8'))
		f.close()

		return feed_value(stats=feed_value(**sim_vars))

