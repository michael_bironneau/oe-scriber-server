from abc import abstractmethod, abstractproperty

class feed():
    """Abstract class representing a 'feed', that is, an entity that emits messages to a particular topic. At the very least, if you want to write one, you should override the following properties/methods::

    :topic:  The name of the topic you want to publish (should be unique)
    :timer:  How often to update the topic (i.e. how often to call update())
    :update:  Main workhorse method to update the feed. It should set the __val property to something JSON-dumpable.
    """
    def __init__(self):
        #Default initialization code
        self.value = None #Latest message in feed
        self.always_publish = True #Whether to publish all messages whether self.value changes or not

    @abstractproperty
    def topic(self):
        pass

    @abstractproperty
    def timer(self):
        pass

    @abstractmethod
    def update(self):
        pass

    def get_latest(self, callback, count_subs):
        """Updates feed if there are subscribers.
        :callback:  Where to send messages once we're done with them
        :count_subs:  Method that returns number of subscribers
        """
        if count_subs(self.topic) > 0:
            v = self.update()
            if v is not None:
                self.value = v
                print('Updated feed "' + self.topic + '"')
                if isinstance(self.value, feed_value):
                    for k,v in self.value.values.items():
                        callback(self.topic + '.' + k, v)
                else:
                    callback(self.topic, self.value)
            else:
                print('No change to feed "' + self.topic + '"')

    def replay(self):
        """Replay current message values to new subscriber"""
        if self.value is not None:
            if isinstance(self.value, feed_value):
                for k,v in self.value.values.items():
                    yield (self.topic + '.' + k, v)
            else:
                yield (self.topic, self.value)

class feed_value():
    """Abstracts the return value of a feed. Can accept nested results. 

    Examples: if self.topic = 'available',

        v = feed_value(high=10, low=15) 

        creates two values: available.high = 10, and available.low = 15. Arbitrarily deep nesting can be achieved:

        v = feed_value(high=feed_value(**client_available_high), low=feed_value(**client_available_low))

        where client_available_high is a dictionary of {Client: Availability}
    """
    def __init__(self, **kwargs):
        """Init from dict of values or feed_value"""
        self.values = {}
        self.add(**kwargs)

    def add(self, **kwargs):
        """Adds values to feed output"""
        for k,v in kwargs.items():
            for subtopic, subvalue in feed_value.construct(k,v):
                self.values[subtopic] = subvalue        

    def construct(key, value):
        """Recursively construct values"""
        if isinstance(value, feed_value):
            #value will have been initialized, so we can access .values
            for k,v in value.values.items():
                yield from feed_value.construct(str(key) + '.' + str(k), v)
        else:
            yield (str(key), value)
