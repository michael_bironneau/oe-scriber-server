import datetime
from abc import abstractproperty
from feeds.__base import feed, feed_value
import openenergi as oe

class swr_feed(feed):

	def getTopic(self):
		return 'switchRequests'
	def getTimer(self):
		return 5

	topic = abstractproperty(fget=getTopic)
	timer = abstractproperty(fget=getTimer)

	def __init__(self):
		super().__init__()
		self.db = oe.g3()
		self.loadeventid = None
		self.initial_query = 'Select top 1 LoadEventId from LoadEvent where timestamp > dateadd(minute, -10, getutcdate())'
		self.update_query = 'Select top 1 LoadEventId from LoadEvent Order by LoadEventId desc'
		self.data_query = """Select Client.ClientCode, Site.Name as 'SiteName', Load.Name as 'LoadName', CONVERT(varchar(19), Timestamp, 121) as Timestamp, Case when Value = -1 then 'Low' else 'High' end as 'RequestType' From LoadEvent with (Nolock) 
						Inner Join Load with (Nolock) on Load.LoadId = LoadEvent.LoadId
						Inner Join Device with (Nolock) on Device.DeviceId = Load.DeviceId
						Inner Join Site with (Nolock) on Site.SiteId = Device.SiteId
						Inner Join Client with (Nolock) on Client.Clientid = Site.ClientId
						Where LoadEventId > <<LOADEVENTID>>
						and LoadEventTypeId = 6 and value <> 0
						Order by Client.ClientCode, Site.Name, Timestamp"""

	def update(self):
		if self.loadeventid is None:
			#Initial update
			lid = int(self.db.sql(self.initial_query).values[0][0])
			self.loadeventid = 0
		else:
			#Incremental update
			lid = int(self.db.sql(self.update_query).values[0][0])

		if lid > self.loadeventid:
			#Returns feed of form switchrequest.[client code] where values are of form {Site Name: [[Load Name, Timestamp, Type]]}
			if self.loadeventid > 0:
				d = to_dict(self.db.sql(self.data_query.replace('<<LOADEVENTID>>', str(self.loadeventid))))
			else:
				d = to_dict(self.db.sql(self.data_query.replace('<<LOADEVENTID>>', str(lid))))
			self.loadeventid = lid
			if len(d) > 0:
				return feed_value(**d)

def to_dict(frame):
	"""Convert shallowly nested pandas dataframe into dictionary, grouping by first column"""
	return {k: g.values.tolist() for k,g in frame.groupby(frame.columns[0])}