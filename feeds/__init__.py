#This should contain a list of feed modules that you want to import. For example,
#if you only want to import Feed1 and Feed2, change the following code to say:
#from . import Feed1, Feed2
from . import time, online_sites, controllable_loads, frequency, provided, available, hotspots, swr, localsim