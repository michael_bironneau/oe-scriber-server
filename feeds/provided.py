import openenergi as oe
from abc import abstractproperty
from feeds.__base import feed

class provided_feed(feed):

	def __init__(self):
		super().__init__()
		self.db = oe.g3()
		self.SQL = """Select Sum(Sub.Resp) from 
					(Select Top 2 ResponseHigh + ResponseLow as 'Resp' from BucketLoadAggregation 
						Where BucketId in (1,9) and AggregationTypeId =2 
						order by timestamp desc) sub
				"""

	def getTopic(self):
		return 'provided'
	def getTimer(self):
		return 10

	topic = abstractproperty(fget=getTopic)
	timer = abstractproperty(fget=getTimer)

	def update(self):
		v = self.db.sql(self.SQL).values[0][0]
		return float(v)