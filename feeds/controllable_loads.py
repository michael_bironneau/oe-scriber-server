import openenergi as oe
from abc import abstractproperty
from feeds.__base import feed

class controllable_loads_feed(feed):

	def __init__(self):
		super().__init__()
		self.db = oe.g3()
		self.SQL = """with loads as (select LoadId from vw_ActiveLoad with (Nolock) where IsReportingToGrid = 1)
					select sum(vw_LoadAvailability.Available) from loads
					inner join vw_LoadAvailability with (nolock) on 
						vw_LoadAvailability.LoadId = loads.LoadId 
						and vw_LoadAvailability.Timestamp = (Select Top 1 Timestamp from 
																vw_LoadAvailability where 
																	LoadId = loads.loadId 
																order by timestamp desc)
					"""

	def getTopic(self):
		return 'controllableLoads'
	def getTimer(self):
		return 10

	topic = abstractproperty(fget=getTopic)
	timer = abstractproperty(fget=getTimer)

	def update(self):
		v = self.db.sql(self.SQL).values[0][0]
		return int(float(v))