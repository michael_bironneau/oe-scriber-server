import openenergi as oe
from abc import abstractproperty
from feeds.__base import feed

class frequency_feed(feed):

	def __init__(self):
		super().__init__()
		self.db = oe.g3()
		self.SQL = "Select Top 1 Frequency from dbo.vw_UKFrequency order by Timestamp desc"

	def getTopic(self):
		return 'frequency'
	def getTimer(self):
		return 10

	topic = abstractproperty(fget=getTopic)
	timer = abstractproperty(fget=getTimer)

	def update(self):
		v = self.db.sql(self.SQL).values[0][0]
		return float(v)